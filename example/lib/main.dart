import 'package:flutter/material.dart';

import 'package:b2_sdk_flutter/b2_sdk_flutter.dart';

import 'package:flutter/services.dart' show rootBundle;
import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'package:crypto/crypto.dart';
import 'package:path_provider/path_provider.dart';

import 'package:path/path.dart' as p;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  Future b2Test() async {
    final bucket = "5d1adaa935fab8596fdd0312";

    B2().accountId = "daa95a89fd32";
    B2().applicationKey = "000a42c1fbcb0b145245abf5da66631003e117efa2";

    var fileName = "test33efefefefef333344.png";
    var directory = await getTemporaryDirectory();
    String localPath = p.join(directory.path, fileName);

    //test
    ByteData byteData = await rootBundle.load("assets/img/userImage.png");
    String testsha1Checksum = sha1.convert(byteData.buffer.asUint8List()).toString();
    File testFile = File(localPath);
    File testWriteFile = await testFile.writeAsBytes(byteData.buffer.asUint8List());
    String testFilePath = testWriteFile.path;
    localPath = testFilePath;

    File file = File(localPath);
    String sha1Checksum = sha1.convert(await file.readAsBytes()).toString();
    int fileSize = await file.length();

//    UriData uriData = UriData.fromUri(Uri.parse(localPath));
//    print(uriData.mimeType);

    print(testsha1Checksum);
    print(sha1Checksum);

//    B2File b2file = await B2().uploadFile(b2uploadUrl, localPath, fileName, contentType: "image/png", contentLength: fileSize);
    B2File b2file = await B2().uploadFileData(byteData.buffer.asUint8List(), fileName, bucket);

    if (b2file == null) {

    } else {

    }

    print(b2file);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.display1,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: b2Test,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
