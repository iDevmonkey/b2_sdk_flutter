import 'dart:async';
import 'dart:typed_data';
import 'package:http/http.dart' as h;

abstract class IB2Agent {
  static Future<String> onResponse(h.Response response) async {
    if (response.statusCode == 200) {
      return response.body;
    }
    else {
      throw ArgumentError.value(response.statusCode, response.reasonPhrase);
    }
  }

  static Future<String> httpUploadFileData(Uri url, Map<String, String> headers, Uint8List fileData) async {
    var request = h.Request("POST", url);
    request.headers.addAll(headers);
    request.bodyBytes = fileData;

    var response = await h.Response.fromStream(await request.send());
    return onResponse(response);
  }
}