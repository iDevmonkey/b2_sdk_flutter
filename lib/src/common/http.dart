//import 'dart:async';
//import 'dart:convert';
//import 'dart:typed_data';
//import 'package:http/http.dart' as sh;

class B2Http {
  static final B2Http _instance = B2Http._();
  B2Http._();

  factory B2Http() {
    return _instance;
  }

  IHttpClient _httpClient;
  IHttpClient get httpClient {
    if (_httpClient == null) {
      _httpClient = buildHttpClient();
    }
    return _httpClient;
  }

  IHttpClient buildHttpClient() => StandardHttpClient();
}

///
/// Client

abstract class IHttpClient {

}

class StandardHttpClient extends IHttpClient {
}

class DioHttpClient extends IHttpClient {
}

