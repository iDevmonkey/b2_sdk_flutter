import 'dart:convert';

class B2Account {
  String accountId;
  String authorizationToken;
  String apiUrl;
  String downloadUrl;

  B2Account._();

  void readFrom(Map map) {
    if (map == null) return;
    this.accountId = map[accountIdKey];
    this.authorizationToken = map[authorizationTokenKey];
    this.apiUrl = map[apiUrlKey];
    this.downloadUrl = map[downloadUrlKey];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map[accountIdKey] = this.accountId;
    map[authorizationTokenKey] = this.authorizationToken;
    map[apiUrlKey] = this.apiUrl;
    map[downloadUrlKey] = this.downloadUrl;
    return map;
  }

  static B2Account fromMap(Map map) {
    B2Account m = B2Account._();
    m.readFrom(map);
    return m;
  }

  static B2Account fromString(String string) {
    if (string == null || string.isEmpty) return null;

    Map map = jsonDecode(string);
    return fromMap(map);
  }

  @override
  String toString() {
    return jsonEncode(toMap());
  }

  static const String accountIdKey = "accountId";
  static const String authorizationTokenKey = "authorizationToken";
  static const String apiUrlKey = "apiUrl";
  static const String downloadUrlKey = "downloadUrl";
}

class B2UploadUrl {
  String bucketId;
  String authorizationToken;
  String uploadUrl;

  B2UploadUrl._();

  void readFrom(Map map) {
    if (map == null) return;
    this.bucketId = map[bucketIdKey];
    this.authorizationToken = map[authorizationTokenKey];
    this.uploadUrl = map[uploadUrlKey];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map[bucketIdKey] = this.bucketId;
    map[authorizationTokenKey] = this.authorizationToken;
    map[uploadUrlKey] = this.uploadUrl;
    return map;
  }

  static B2UploadUrl fromMap(Map map) {
    B2UploadUrl m = B2UploadUrl._();
    m.readFrom(map);
    return m;
  }

  static B2UploadUrl fromString(String string) {
    if (string == null || string.isEmpty) return null;

    Map map = jsonDecode(string);
    return fromMap(map);
  }

  @override
  String toString() {
    return jsonEncode(toMap());
  }

  static const String bucketIdKey = "bucketId";
  static const String authorizationTokenKey = "authorizationToken";
  static const String uploadUrlKey = "uploadUrl";
}

class B2File {
  String fileId;
  String fileName;
  String accountId;
  String bucketId;
  int contentLength;
  String contentSha1;
  String contentType;

  B2File._();

  void readFrom(Map map) {
    if (map == null) return;
    this.fileId = map[fileIdKey];
    this.fileName = map[fileNameKey];
    this.accountId = map[accountIdKey];
    this.bucketId = map[bucketIdKey];
    this.contentLength = map[contentLengthKey];
    this.contentSha1 = map[contentSha1Key];
    this.contentType = map[contentTypeKey];
  }

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map[fileIdKey] = this.fileId;
    map[fileNameKey] = this.fileName;
    map[accountIdKey] = this.accountId;
    map[bucketIdKey] = this.bucketId;
    map[contentLengthKey] = this.contentLength;
    map[contentSha1Key] = this.contentSha1;
    map[contentTypeKey] = this.contentType;
    return map;
  }

  static B2File fromMap(Map map) {
    B2File m = B2File._();
    m.readFrom(map);
    return m;
  }

  static B2File fromString(String string) {
    if (string == null || string.isEmpty) return null;

    Map map = jsonDecode(string);
    return fromMap(map);
  }

  @override
  String toString() {
    return jsonEncode(toMap());
  }

  static const String fileIdKey = "fileId";
  static const String fileNameKey = "fileName";
  static const String accountIdKey = "accountId";
  static const String bucketIdKey = "bucketId";
  static const String contentLengthKey = "contentLength";
  static const String contentSha1Key = "contentSha1";
  static const String contentTypeKey = "contentType";
}