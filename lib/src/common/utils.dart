import 'dart:io';
import 'dart:convert';
import 'dart:typed_data';
import 'dart:math';
import 'package:crypto/crypto.dart';
import 'package:mime/mime.dart';

import 'defines.dart';

class B2Utils {
  static String authorizeAccountPayload(String accountId, String applicationKey) {
    String credentials = "$accountId:$applicationKey";
    String encodeCredentials = base64Encode(utf8.encode(credentials));
    return "Basic $encodeCredentials";
  }

  static Map<String, String> uploadFileHeaders(String authorizationToken, String filename, {String sha1Checksum, String contentType, int contentLength}) {
    Map<String, String> headers = {
      B2HTTPHeaders.authorization: authorizationToken,
      B2HTTPHeaders.fileName: filename,
      B2HTTPHeaders.contentSHA1: sha1Checksum ?? B2HTTPHeaders.valueNotVerifyOfContentSHA1,
      B2HTTPHeaders.contentType: contentType ?? B2HTTPHeaders.valueAutoOfContentType
    };

    if (contentLength != null) {
      headers[B2HTTPHeaders.contentLength] = contentLength.toString();
    }

    return headers;
  }

  static Future<Map<String, String>>httpHeadersForFilePath(String filePath) async {
    File file = File(filePath);
    return _httpHeadersForFile(await file.readAsBytes(), filePath: filePath);
  }

  static Future<Map<String, String>> httpHeadersForFileData(Uint8List fileData) async {
    return _httpHeadersForFile(fileData);
  }

  static Future<Map<String, String>> _httpHeadersForFile(Uint8List fileData, {String filePath}) async {
    Map<String, String> httpHeaders = Map<String, String>();

    int length = fileData.length;
    Uint8List headerOfFile = fileData.sublist(0, min(defaultMagicNumbersMaxLength, length));
    String mimeType = lookupMimeType(filePath ?? "", headerBytes: headerOfFile);

    String sha1Checksum = sha1.convert(fileData).toString();

    if (mimeType != null && mimeType.isNotEmpty) {
      httpHeaders[B2HTTPHeaders.contentType] = mimeType;
    }

    if (sha1Checksum != null && sha1Checksum.isNotEmpty) {
      httpHeaders[B2HTTPHeaders.contentSHA1] = sha1Checksum;
    }

    return httpHeaders;
  }

  static int getArgumentErrorCode(dynamic e) {
    if (e == null || !(e is ArgumentError)) return null;

    var invalidValue = (e as ArgumentError).invalidValue;
    if (invalidValue == null || !(invalidValue is int)) return null;
    return invalidValue;
  }
}