import 'dart:async';
import 'dart:convert';
import 'dart:typed_data';
import 'package:http/http.dart' as h;

import 'common/base.dart';
import 'common/models.dart';
import 'common/defines.dart';
import 'common/utils.dart';

class B2UploadAgent extends IB2Agent {
  static Future<B2UploadUrl> getUploadUrl(B2Account account, String bucketId) async {
    Uri url = Uri.parse(B2Urls.getUploadUrl(account.apiUrl));
    Map<String, String> headers = {B2HTTPHeaders.authorization: account.authorizationToken};
    String bodyString = jsonEncode({"bucketId": bucketId});

    var response = await h.Client().post(url, headers: headers, body: utf8.encode(bodyString));
    var result = await IB2Agent.onResponse(response);
    return B2UploadUrl.fromString(result);
  }

  static Future<B2File> uploadFileWithPath(B2UploadUrl uploadUrl, String filePath, String filename, {String sha1Checksum, String contentType, int contentLength}) async {
    Uri url = Uri.parse(uploadUrl.uploadUrl);
    Map<String, String> headers = B2Utils.uploadFileHeaders(uploadUrl.authorizationToken, filename, sha1Checksum: sha1Checksum, contentType: contentType, contentLength: contentLength);

//    var file = await h.MultipartFile.fromPath("file", filePath, filename: filename);
//    var request = h.MultipartRequest("POST", url);
//    request.headers.addAll(headers);
//    request.files.add(file);
//
//    var response = await h.Response.fromStream(await request.send());
//    print(response.body);
//    print(jsonDecode(response.body));
//    if (response.statusCode == 200) {
//      return B2File.fromString(response.body);
//    }
//    return null;

    return null;
  }

  static Future<B2File> uploadFileWithData(B2UploadUrl uploadUrl, Uint8List fileData, String filename, {String sha1Checksum, String contentType, int contentLength}) async {
    Uri url = Uri.parse(uploadUrl.uploadUrl);
    Map<String, String> headers = B2Utils.uploadFileHeaders(uploadUrl.authorizationToken, filename, sha1Checksum: sha1Checksum, contentType: contentType, contentLength: contentLength);

    var result = await IB2Agent.httpUploadFileData(url, headers, fileData);
    return B2File.fromString(result);
  }
}